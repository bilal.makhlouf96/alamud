# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import UseEvent

class UseAction(Action2):
	EVENT = UseEvent
	ACTION = "use"
	RESOLVE_OBJECT = "resolve_for_use"
